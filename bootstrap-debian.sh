#!/bin/sh
# bootstrap-debian.sh <suite> <target_dir>

[ $# -eq 2 ] || exit 1

suite="$1"
target="$2"

[ -n "$DISPLAY" ] && xset s off -dpms # turn off screensaver

dpkg-query -W mmdebstrap > /dev/null || sudo apt-get install mmdebstrap

sudo mmdebstrap \
  --variant=standard \
  --include=python3-venv,sudo \
  --setup-hook=/usr/share/mmdebstrap/hooks/setup00-merged-usr.sh \
  "$suite" "$target" "\
  deb http://deb.debian.org/debian $suite main contrib non-free
  deb http://deb.debian.org/debian $suite-updates main contrib non-free
  deb http://deb.debian.org/debian-security $suite-security main contrib non-free
  deb http://deb.debian.org/debian $suite-backports main contrib non-free"
