user_pref("browser.urlbar.suggest.topsites", false);
user_pref("extensions.pocket.enabled", false);
user_pref("keyword.enabled", true);
user_pref("network.cookie.lifetimePolicy", 2);
user_pref("network.http.referer.XOriginPolicy", 0);
user_pref("privacy.clearOnShutdown.cookies", false);
